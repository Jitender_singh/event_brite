from django.views import generic
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
import requests

# Create your views here.

EVENT_URL = "https://www.eventbriteapi.com/v3/events/search/"
CATEGORIES_URL = "https://www.eventbriteapi.com/v3/categories/"


class CategoriesSelectionView(generic.TemplateView):
    template_name = 'get_data/category_select.html'

    def get_context_data(self, **kwargs):
        # overwrite the get_context_data
        context = super(CategoriesSelectionView, self).get_context_data(**kwargs)
        context['token'] = settings.EVENTBRITE_TOKEN  # set EventBrite Token
        context['categories'] = self.get_categories()['categories']  # set all categories
        return context

    @staticmethod
    def get_categories(page=None):
        """
        Get all the categories and return it into the json.
        """
        token = settings.EVENTBRITE_TOKEN
        categories_url = "%s?token=%s&page=%s" % (CATEGORIES_URL, token, page)
        data = requests.get(categories_url)
        return data.json()


class EventDetailView(generic.TemplateView):
    template_name = 'get_data/event_detail.html'

    def get_context_data(self, **kwargs):
        # overwrite the get_context_data
        context = super(EventDetailView, self).get_context_data(**kwargs)
        categories = self.request.GET.get('categories', None)
        context['event_data'] = None
        context['categories'] = categories
        context['page_url'] = "%s?categories=%s" % (reverse_lazy('get_data:event_detail'), categories)
        if categories:
            page = self.request.GET.get('page', None)
            context['event_data'] = self.get_event_data(page, categories)  # set event data
        return context

    @staticmethod
    def get_event_data(page, categories):
        """
        Get all the events of categories.
        """
        token = settings.EVENTBRITE_TOKEN
        event_url = "%s?token=%s&categories=%s&page=%s" % (EVENT_URL, token, categories, page)
        data = requests.get(event_url)
        return data.json()