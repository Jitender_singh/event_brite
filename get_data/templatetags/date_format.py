from django import template
from datetime import datetime
register = template.Library()


@register.simple_tag
def date_time(date_str, date_format="%m/%d/%Y %H:%M:%S", given_string_format="%Y-%m-%dT%H:%M:%S"):
    """
    Template tags used for date time formatting.
    It accept the DateTime String in '%Y-%m-%dT%H:%M:%S' format
    And Convert it into the other given date_format "%m/%d/%Y %H:%M:%S"
    """
    datetime_obj = convert_datetime_obj(date_str)  # convert into datetime object
    if datetime_obj:
        return datetime_obj.strftime(date_format)
    return "None"


def convert_datetime_obj(date_str, string_format="%Y-%m-%dT%H:%M:%S"):
    """
    Convert the string to date time format.
    """
    try:
        return datetime.strptime(date_str, string_format)
    except ValueError, TypeError:
        return False