function simpleAjaxCall(method, url, headers, data) {

    method = (method == "undefined" || method == null) ? 'GET' : method;
    // Return the ajax object
    // By default it use 'GET' ajax call
    return $.ajax({
        method: method,
        url: url,
        data: data,
        headers: headers,
        dataType: 'json'
    });

}


function showLoader(ele){
    $(ele).html("<div class='text-center'><img src='/static/img/loader.gif' alt='loader'/><p>Loading...</p></div>");
}