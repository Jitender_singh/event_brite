(function(){

    $(document).ready(function(){
            var ele_class = ".paginate",
                total_page = $(ele_class).attr('data-total-page'),
                current_page = $(ele_class).attr('data-current-page'),
                page_url = $(ele_class).attr('data-url');


            $(ele_class).bootpag({
                total: total_page,
                page: current_page,
                maxVisible: 10,
                leaps: true,
                firstLastUse: true,
                first: '←',
                last: '→'
                }).on("page", function(event, num){
                    window.location = page_url+"&page="+num;
            });

    });

})();
