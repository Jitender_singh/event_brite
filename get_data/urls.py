from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'event_brite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^event/', views.EventDetailView.as_view(), name="event_detail"),
    url(r'^$', views.CategoriesSelectionView.as_view(), name="select_categories"),
]


