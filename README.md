### INTRODUCTION

This is django project. This project uses the eventbrite API and show the categories events detail.

### HOW TO INSTALL

Run following commands:-

> pip install -r requirement.txt

> python manage.py syncdb

> python manage.py runserver
